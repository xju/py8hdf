# Py8HDF

Pythia8 Showering for Hmumu Fast simulation

```
docker build -t docexoty/py8hdf:0.0.1 .
docker push docexoty/py8hdf:0.0.1
```
Then build the one for Cori
```
cd Cori
docker build -t docexoty/py8hdf-cori:0.0.1 .
docker push docexoty/py8hdf-cori:0.0.1
```

Run on Cori
```
shifterimg pull docexoty/py8hdf-cori:0.0.1

[interactive mode]
shifter --image=docexoty/py8hdf-cori:0.0.1 /bin/bash

shifter --image=docexoty/py8hdf-cori:0.0.1  mpirun -np 4 withlheh5 -H /global/project/projectdirs/m3253/hschulz/xyj/setups_13TeV/zj/j0/j0-repack.hdf5 -p /global/project/projectdirs/m3253/hschulz/xyj/setups_13TeV/merging_jetmax0.cmd -a ATLAS_2017_I1599399 -n 10000 -o j0_jetmax0.yoda

[single node]
salloc -N 1 -q interactive -C haswell -A m3253 --image=docexoty/py8hdf-cori:0.0.1 -t 00:30:00 srun -n 4 shifter withlheh5 -H /global/project/projectdirs/m3253/hschulz/xyj/setups_13TeV/zj/j0/j0-repack.hdf5 -p /global/project/projectdirs/m3253/hschulz/xyj/setups_13TeV/merging_jetmax0.cmd -a ATLAS_2017_I1599399 -n 10000 -o j0_jetmax0.yoda
```

[NERSC Tutorial](https://docs.nersc.gov/programming/shifter/how-to-use)

Batch Job Script
```
#!/bin/bash
#SBATCH --image=docker:docexoty/py8hdf-cori:0.0.1
#SBATCH --nodes=1
#SBATCH --qos=regular

srun -n 32 shifter withlheh5 -H j0-repack.hdf5 -p merging_jetmax0.cmd -a ATLAS_2017_I1599399 -n 10000 -o j0_jetmax0.yoda
```


Existing NV docker images, [NGC description](https://docs.nvidia.com/deeplearning/frameworks/support-matrix/index.html#unique_1062232252), [NGC names](https://ngc.nvidia.com/catalog/containers/nvidia:tensorflow/tags).
