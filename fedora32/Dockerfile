FROM fedora:31

RUN dnf update -y && dnf -y install \
	autoconf automake make gcc gcc-c++ gcc-gfortran redhat-rpm-config \
	wget tar less bzip2 findutils which zlib-devel \
	python python-devel python-requests python-matplotlib  python3-sphinx  \
	ghostscript ImageMagick  \
	cmake cmake3 git vim libtool rsync gzip pip patchelf \
	eigen3-devel xtl-devel boost boost-devel hdf5-mpich-devel \
	doxygen xerces-c expat-devel xerces-c-devel subversion\
	tbb tbb-devel  \
	binutils libX11-devel libXpm-devel libXft-devel libXext-devel \
	mysql-devel fftw-devel cfitsio-devel graphviz-devel \
	openldap-devel pcre-devel \
	file gsl-devel libxml2-devel libuuid-devel nodejs \
	mesa-libGL-devel mesa-libGLU-devel ftgl-devel glew-devel openssl-devel \
	&& dnf clean all


# root-6.22.08, 2021-03-12
ENV ROOT_VERSION="6.20.04"
RUN mkdir /code && cd /code \
	&& wget https://root.cern/download/root_v${ROOT_VERSION}.source.tar.gz -O- | tar xz \
	&& mkdir root_builder && cd root_builder \
	&& cmake -DCMAKE_INSTALL_PREFIX=/usr/local/root ../root-${ROOT_VERSION} -Dminuit2=ON -Dcxx17=ON -Dgviz=ON -Drpath=ON -Dgnuinstall=ON \
	&& cmake --build . --target install -- -j8 \
	&& cd  && rm -rf /code && echo "source /usr/local/root/bin/thisroot.sh" >> ~/.bashrc

ENV DD4HEP_VERSION="01-11-02"
ENV HEPMC3_VERSION="3.2.1"

ENV RIVET_ANALYSIS_PATH="/usr/local/share/RivetAnalyses"
RUN mkdir -p $RIVET_ANALYSIS_PATH
ENV PYTHONUSERBASE="/usr/local/python3.7"
ENV LD_LIBRARY_PATH="/usr/lib64/mpich/lib:/usr/local/lib:${LD_LIBRARY_PATH}"

# RUN source /usr/local/root/bin/thisroot.sh
# pylint, root_numpy
RUN pip install --upgrade pip && pip install --user \
		pylint root_numpy jupyterlab jax jaxlib h5py iminuit \
		vaex sklearn scipy numba mpi4py pyDOE2 autograd tables tqdm Gpy>=1.9.9 \
	&& jupyter lab build

# LHAPDF-6.2.3
RUN mkdir /code && cd /code \
    && wget https://lhapdf.hepforge.org/downloads/?f=LHAPDF-6.2.3.tar.gz -O- | tar xz \
    && cd LHAPDF-*/ && ./configure --prefix=/usr/local \
    && make -j4 && make install \
    && cd ../.. && rm -r /code

# xtensor-0.21.5
RUN mkdir /code && cd /code \
	&& wget https://github.com/xtensor-stack/xtensor/archive/0.21.5.tar.gz -O- | tar xz \ 
	&& cd xtensor-0.21.5 \
	&& cmake . -DCMAKE_INSTALL_PREFIX=/usr/local \
		-DCMAKE_CXX_COMPILER=`which g++` -DCMAKE_C_COMPILER=`which gcc`  \
	&& make install \
    && cd ../.. && rm -r /code

# HighFive-master
RUN mkdir /code && cd /code \
	&& git clone https://github.com/BlueBrain/HighFive.git \
	&& cd HighFive && mkdir build && cd build \
	&& cmake .. -DCMAKE_INSTALL_PREFIX=/usr/local \
		-DCMAKE_CXX_COMPILER=`which g++` -DCMAKE_C_COMPILER=`which gcc` \
		-DHIGHFIVE_EXAMPLES=OFF -DHIGHFIVE_UNIT_TESTS=OFF \
		-DHIGHFIVE_USE_XTENSOR=ON -DHIGHFIVE_USE_EIGEN=ON \
	&& make && make install \
    && cd ../.. && rm -r /code

# lhef5-0.3.0
RUN mkdir /code && cd /code \
	&& wget https://bitbucket.org/iamholger/lheh5/downloads/lheh5-0.3.0.tar.gz -O- | tar xz \
	&& cd lheh5-0.3.0 && mkdir build && cd build \
	&& cmake .. -DHIGHFIVE_DIR=/usr/local -DCMAKE_INSTALL_PREFIX=/usr/local && make && make install \
    && cd ../.. && rm -r /code

# rivet-3.1.0
RUN mkdir /code && cd /code \
    && wget https://gitlab.com/hepcedar/rivetbootstrap/raw/3.1.0/rivet-bootstrap \
    && chmod +x rivet-bootstrap \
    && INSTALL_PREFIX=/usr/local INSTALL_GSL=0 INSTALL_RIVETDEV=0 MAKE="make -j4" ./rivet-bootstrap \
    && echo "source /usr/local/share/Rivet/rivet-completion" > /etc/profile.d/rivet-completion.sh \
    && echo "source /usr/local/share/YODA/yoda-completion" > /etc/profile.d/yoda-completion.sh \
    && rm -rf /code

# HepMC3-3.2.1
RUN mkdir /code && cd /code \
	&& wget http://hepmc.web.cern.ch/hepmc/releases/HepMC3-${HEPMC3_VERSION}.tar.gz -O- | tar xz \
	&& cd HepMC3-* && mkdir build && cd build \	
	&& cmake .. -DHEPMC3_ENABLE_ROOTIO=OFF \
		-DHEPMC3_ENABLE_PYTHON=OFF \	
		-DCMAKE_INSTALL_PREFIX=/usr/local \
		-DHEPMC3_BUILD_EXAMPLES=OFF \
		-DCMAKE_C_COMPILER=`which gcc` \
		-DCMAKE_CXX_COMPILER=`which g++` \
	&& make -j4 && make install \
    && cd ../../ && rm -r /code


# Pythia 8.302
RUN mkdir /code && cd /code \
    && wget http://home.thep.lu.se/~torbjorn/pythia8/pythia8302.tgz -O- | tar xz \
    && cd pythia*/ && ./configure --enable-shared --{prefix,with-{evtgen,fastjet2,hepmc2,lhapdf6,root,python}}=/usr/local\
					  --with-{root,boost}=/usr --with-gzip-include=/usr/include --with-gzip-lib=/usr/lib64 \
					  --with-hepmc3-include=/usr/local/include --with-hepmc3-lib=/usr/local/lib64 \
    && make -j4 && make install \
    && cd ../.. && rm -r /code

RUN lhapdf install NNPDF23_lo_as_0130_qed

# diy
RUN mkdir /code && cd /code \
	&& wget https://portal.nersc.gov/project/atlas/xju/diy.tar.gz -O- | tar xz \
	&& cd diy/include && mv diy /usr/local/include \
	&& cd && rm -rf /code

# clhep
RUN mkdir /code && cd /code \
	&& wget http://proj-clhep.web.cern.ch/proj-clhep/dist1/clhep-2.4.1.3.tgz -O- | tar xz  \
	&& cd 2.4.1.3/CLHEP && mkdir build && cd build \
	&& cmake .. -DCMAKE_INSTALL_PREFIX=/usr/local \
	&& make && make install \
	&& cd ../../.. && rm -r /code

# geant 4
RUN mkdir /code && cd /code \
	&& wget http://geant4-data.web.cern.ch/geant4-data/releases/geant4.10.06.p02.tar.gz -O- | tar xz \
	&& cd geant4.10.06.p02 && mkdir build && cd build \
	&& cmake .. -DCMAKE_INSTALL_PREFIX=/usr/local -DGEANT4_INSTALL_DATA=ON -DGEANT4_USE_GDML=ON \
				-DXERCESC_ROOT_DIR=/usr -DCLHEP_ROOT_DIR=/usr/local \
				# -DGEANT4_BUILD_MULTITHREADED=ON -DGEANT4_USE_QT=ON -DGEANT4_USE_OPENGL_X11=ON \
	&& make -j4 && make install \
    && cd ../.. && rm -r /code

# dd4hep 1.11.02
RUN mkdir /code && cd /code \
 	&& wget https://github.com/AIDASoft/DD4hep/archive/v${DD4HEP_VERSION}.tar.gz -O- | tar xz \
	&& cd DD4hep-${DD$HEPMC3_VERSION} && mkdir build && cd build \ 
	&& cmake .. -DDD4HEP_USE_XERCESC=ON -DDXERCESC_ROOT_DIR=/usr \
			-DDD4HEP_USE_GEANT4=ON -DGeant4_DIR=/usr/local -DBUILD_TESTING=OFF \
			-DCMAKE_INSTALL_PREFIX=/usr/local \
			-DROOT_DIR=/usr/local/root \
	&& make && make install \
    && cd ../.. && rm -r /code

# yodfh5
RUN mkdir /code && cd /code \
	&& wget https://github.com/xju2/yodf5/archive/v0.1.tar.gz -O- | tar xz \
	&& cd yodf5*/ && mkdir build && cd build \
	&& cmake ../src && make -j4 && make install \
	&& cd ../.. && rm -rf /code

# pythia8-diy
RUN mkdir /code && cd /code \
	&& wget https://portal.nersc.gov/project/atlas/xju/pythia8-diy.tar.gz -O- | tar xz \
	&& cd pythia8-diy && mkdir build && cd build \
	&& cmake .. -DHIGHFIVE_DIR=/usr/local \
				-DDIY_INCLUDE_DIRS=/usr/local/include \
				-DYODA_DIR=/usr/local \
				-DRIVET_DIR=/usr/local \
				-DPYTHIA8_DIR=/usr/local \
				-DFASTJET_DIR=/usr/local \
				-DHEPMC_DIR=/usr/local \
				-DCMAKE_PREFIX_PATH=/usr/local/lib/cmake \
	&& make && make install \
	&& cd ../.. && rm -rf /code

# mpituning-0.4.3
RUN mkdir /code && cd /code && VERSION=0.4.3 \
 	&& wget https://gitlab.cern.ch/xju/mpituning/-/archive/v${VERSION}/mpituning-v${VERSION}.tar.gz -O- | tar xz \
	&& cd mpituning-v${VERSION} && pip install . --user \
	&& \cp configs/plotting.py /usr/local/lib64/python3.7/site-packages/yoda/plotting.py \
	&& cd rivet &&  rivet-build --with-root RivetATLAS_2020_I1315949.so ATLAS_2020_I1315949.cc \
	&& cp * /usr/local/share/RivetAnalyses \
    && cd  && rm -r /code
